﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Recognition.CustomAttributes;
using Recognition.Enums;

namespace Recognition.Models
{
    /// <summary>
    /// Описывает модель для отображения в таблице результатов при количественной оценке
    /// </summary>
    internal class ResultViewQuantity
    {
        /// <summary>
        /// Получает или задает способ расчета метрики
        /// </summary>
        [Display(Title = "Тип расстояния", Type = typeof(DistanceType))]
        public DistanceType DistanceType { get; set; }

        /// <summary>
        /// Получает или задает список значений метрики, рассчитаной для каждого из эталонных образов
        /// </summary>
        [Display(Title = "Э", Type = typeof(IEnumerable<double>))]
        public List<double> Distances { get; set; }

        /// <summary>
        /// Получает или задает определенный в результате анализа спектральный класс звезды
        /// </summary>
        [RowHeader]
        [Display(Title = "Класс", Type = typeof(StarClass))]
        public StarClass DefinedClass { get; set; }

        /// <summary>
        /// Создает новый экземпляр типа <see cref="ResultViewQuantity"/>
        /// </summary>
        /// <param name="distanceType">Используемая метрика</param>
        public ResultViewQuantity(DistanceType distanceType)
        {
            this.DistanceType = distanceType;
            this.Distances = new List<double>();
        }
    }

    /// <summary>
    /// Описывает модель для отображения в таблице результатов при качественной оценке
    /// </summary>
    internal class ResultViewQuality
    {
        /// <summary>
        /// Получает или задает используемую функцию сходства
        /// </summary>
        [Display(Title = "Тип расстояния")]
        public SimilarityFunction SimilarityFunction { get; set; }

        /// <summary>
        /// Получает или задает список значений функции сходства, рассчитаной для каждого из эталонных образов
        /// </summary>
        [Display(Title = "Э")]
        public List<double> Similarity { get; set; }

        /// <summary>
        /// Получает или задает определенный в результате анализа класс вещества
        /// </summary>
        [Display(Title = "Класс")]
        public SubstanceClass DefinedClass { get; set; }

        /// <summary>
        /// Создает новый экземпляр типа <see cref="ResultViewQuality"/>
        /// </summary>
        /// <param name="similarityFunction">Используемая функция сходства</param>
        public ResultViewQuality(SimilarityFunction similarityFunction)
        {
            this.SimilarityFunction = similarityFunction;
            this.Similarity = new List<double>();
        }
    }
}