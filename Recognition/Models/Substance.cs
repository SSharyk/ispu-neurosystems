﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Recognition.CustomAttributes;

namespace Recognition.Models
{
    /// <summary>
    /// Описывает модель химического вещества
    /// </summary>
    public class Substance
    {
        /// <summary>
        /// Получает или задает химический класс вещества или его формулу
        /// </summary>
        [Display(Title = "Вещество", Type=typeof(string))]
        public string Class { get; set; }

        /// <summary>
        /// Получает или задает факт: является ли вещество простым
        /// </summary>
        [Display(Title = "Простое вещество", Attribute = "elementary",Type=typeof(bool))]
        public bool IsElementary { get; set; }
        
        /// <summary>
        /// Получает или задает факт: молекула обзательно содержит атом(ы) водорода
        /// </summary>
        [Display(Title = "Есть атомы водорода", Attribute = "hasHydrogen", Type = typeof(bool))]
        public bool HasHydrogen { get; set; }

        /// <summary>
        /// Получает или задает факт: молекула обязательно содержит атом(ы) металла
        /// </summary>
        [Display(Title = "Есть атомы металла", Attribute = "hasMetal", Type = typeof(bool))]
        public bool HasMetal { get; set; }

        /// <summary>
        /// Получает или задает факт: цвет индикатора меняется при взаимодействии с веществом
        /// </summary>
        [Display(Title = "Изменяет цвет индикатора", Attribute = "changesColor", Type = typeof(bool))]
        public bool ChangesColor { get; set; }

        /// <summary>
        /// Получает или задает факт: вещество успешно взаимодействует с кислотами
        /// </summary>
        [Display(Title = "Взаимодействует с кислотами", Attribute = "acidityInteraction", Type = typeof(bool))]
        public bool AcidityInteraction { get; set; }

        /// <summary>
        /// Получает или задает факт: вещество успешно взаимодействует с щелочами
        /// </summary>
        [Display(Title = "Взаимодействует с щелочами", Attribute = "alkaliInteraction", Type = typeof(bool))]
        public bool AlkaliInteraction { get; set; }
        
        /// <summary>
        /// Создает новый экземпляр типа <see cref="Substance"/>
        /// </summary>
        /// <param name="element">Узел XML-документа, хранящий данные вещества</param>
        public Substance(XElement element)
        {
            var properties = this.GetType().GetProperties();
            foreach (var prop in properties)
            {
                var attr = prop.GetCustomAttributes(false)
                    .ToDictionary(a => a.GetType().Name,
                                  a => a as DisplayAttribute);

                if (element.Attribute(attr["DisplayAttribute"].Attribute) == null)
                    prop.SetValue(this, element.Value, null);
                else
                    prop.SetValue(this, Boolean.Parse(element.Attribute(attr["DisplayAttribute"].Attribute).Value), null);
            }
        }

        public Substance(string cls, string isElem, string hasH, string hasMe, string changesColor, string acidityInteraction, string alkaliInteraction)
        {
            this.Class = cls;
            this.IsElementary = bool.Parse(isElem);
            this.HasHydrogen = bool.Parse(hasH);
            this.HasMetal = bool.Parse(hasMe);
            this.ChangesColor = bool.Parse(changesColor);
            this.AcidityInteraction = bool.Parse(acidityInteraction);
            this.AlkaliInteraction = bool.Parse(alkaliInteraction);
        }
    }
}