﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Recognition.CustomAttributes;
using Recognition.Enums;

namespace Recognition.Models
{
    /// <summary>
    /// Описывает модель звезды
    /// </summary>
    public class Star
    {
        /// <summary>
        /// Получает или задает спектральный класс звезды
        /// </summary>
        [RowHeader]
        [Display(Title = "Класс", Type = typeof(StarClass))]
        public StarClass Class { get; set; }

        /// <summary>
        /// Получает или задает максимальную температуру поверхности звезды
        /// </summary>
        [Display(Title = "Температура", Type = typeof(double))]
        public double MaxTemperature { get; set; }

        /// <summary>
        /// Получает или задает цвет звезды
        /// </summary>
        [Display(Title = "Цвет", Type = typeof(Color))]
        public Color Color { get; set; }

        /// <summary>
        /// Получает или задает массу звезды
        /// </summary>
        [Display(Title = "Масса", Type = typeof(double))]
        public double Mass { get; set; }

        /// <summary>
        /// Получает или задает радиус звезды
        /// </summary>
        [Display(Title = "Радиус", Type = typeof(double))]
        public double Radius { get; set; }

        /// <summary>
        /// Получает или задает светимость звезды
        /// </summary>
        [Display(Title = "Светимость", Type = typeof(double))]
        public double Luminosity { get; set; }

#if DEBUG
        /// <summary>
        /// Узел XML-документа, хранящий данные звезды
        /// </summary>
        private XElement _element;
#endif

        /// <summary>
        /// Создает новый экземпляр типа <see cref="Star"/>
        /// </summary>
        /// <param name="element">Узел XML-документа, хранящий данные звезды</param>
        public Star(XElement element)
        {
#if DEBUG
            this._element = element;
#endif
            /// TODO: make custom exceptions for NoAttribute; InvalidAttributeValue, etc
            this.MaxTemperature = Double.Parse(element.Attribute("max-temperature").Value);
            this.Mass = Double.Parse(element.Attribute("mass").Value);
            this.Radius = Double.Parse(element.Attribute("radius").Value);
            this.Luminosity = Double.Parse(element.Attribute("luminosity").Value);
            this.Color = ColorTranslator.FromHtml(element.Attribute("color").Value);
            this.Class = (StarClass) Enum.Parse(typeof (StarClass), element.Value);
        }

        /// <summary>
        /// Создает новый экземпляр типа <see cref="Star"/>
        /// </summary>
        /// <param name="starClass">Спектральный класс</param>
        /// <param name="temperature">Температура поверхности</param>
        /// <param name="color">Истинный</param>
        /// <param name="mass">Масса</param>
        /// <param name="radius">Радиус</param>
        /// <param name="luminosity">Светимость</param>
        public Star(string starClass, string temperature, string color, string mass, string radius, string luminosity)
        {
            this.Class = (StarClass)Enum.Parse(typeof(StarClass), starClass);
            this.MaxTemperature = double.Parse(temperature);
            this.Color = ColorTranslator.FromHtml(color);
            this.Mass = double.Parse(mass);
            this.Radius = double.Parse(radius);
            this.Luminosity = double.Parse(luminosity);
        }

#if DEBUG
        /// <summary>
        /// Возвращает строковое представление элемента
        /// </summary>
        /// <returns>Строка XML-документа</returns>
        public override string ToString()
        {
            return _element.ToString();
        }
#endif
    }
}