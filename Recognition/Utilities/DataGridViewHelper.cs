﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Recognition.CustomAttributes;

namespace Recognition.Utilities
{
    public static class DataGridViewHelper
    {
        #region Table filling

        /// <summary>
        /// Создает таблицу
        /// </summary>
        /// <typeparam name="T">Тип модели</typeparam>
        /// <param name="dataGrid">Таблица для отображения</param>
        /// <param name="data">Значения, которые должны быть занесены в строки таблицы</param>
        public static void GenerateTable<T>(this DataGridView dataGrid, IEnumerable<T> data)
        {
            dataGrid.Rows.Clear();
            dataGrid.Columns.Clear();
            
            // columns
            var modelProperties = typeof (T).GetProperties();
            PropertyInfo header = null;
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                var attr = modelProperty.GetCustomAttributes(false)
                    .ToDictionary(a => a.GetType().Name, a => a as DisplayAttribute);

                if (modelProperty.PropertyType.Name == "List`1")
                {
                    var vals = modelProperty.GetValue(data.ElementAt(0), null) as IEnumerable;

                    int i = -1;
                    foreach (var listItem in vals)
                    {
                        i++;
                        dataGrid.Columns.Add(modelProperty.Name + (i + 1), attr["DisplayAttribute"].Title + (i + 1));
                        dataGrid.Columns[modelProperty.Name + (i + 1)].Width = 35;
                    }
                    continue;
                }

                dataGrid.Columns.Add(modelProperty.Name,
                                     attr["DisplayAttribute"].Title);
                if (attr.ContainsKey("RowHeaderAttribute"))
                {
                    header = modelProperty;
                    dataGrid.Columns[modelProperty.Name].Width = 50;
                }
            }

            // rows 
            for (int it = 0; it < data.Count(); it++)
            {
                var item = data.ElementAt(it);
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(dataGrid);
                List<object> values = new List<object>();
                int cellNumber = -1;
                foreach (PropertyInfo modelProperty in modelProperties)
                {
                    cellNumber++;
                    if (modelProperty.PropertyType.IsEnum)
                    {
                        values.Add(modelProperty.GetValue(item, null).ToString().Decapitalize());
                        continue;
                    }
                    if (modelProperty.PropertyType == typeof (Color))
                    {
                        values.Add(ColorTranslator.ToHtml((Color) (modelProperty.GetValue(item, null))));
                        row.Cells[cellNumber].Style.BackColor = (Color) (modelProperty.GetValue(item, null));
                        continue;
                    }
                    if (modelProperty.PropertyType.Name == "List`1")
                    {
                        var vals = (modelProperty.GetValue(item, null) as IEnumerable);
                        cellNumber--;
                        foreach (var listItem in vals)
                        {
                            cellNumber++;
                            values.Add(listItem);
                        }
                        continue;
                    }

                    values.Add(modelProperty.GetValue(item, null));
                }

                //row.HeaderCell=new DataGridViewRowHeaderCell();
                //row.HeaderCell.Value = header.GetValue(etalon, null);
                row.SetValues(values.ToArray());
                dataGrid.Rows.Add(row);
            }
        }
        
        /// <summary>
        /// Создает таблицу
        /// </summary>
        /// <typeparam name="T">Тип модели</typeparam>
        /// <param name="dataGrid">Таблица для отображения</param>
        /// <param name="row">Данные, которые должны быть занесены в качестве строки таблицы</param>
        public static void GenerateTable<T>(this DataGridView dataGrid, T row)
        {
            dataGrid.GenerateTable(new List<T> {row}.AsEnumerable());
        }

        #endregion

        #region Rows parsing

        /// <summary>
        /// Получает объект модели из строки таблицы
        /// </summary>
        /// <typeparam name="T">Тип модели</typeparam>
        /// <param name="dataGrid">Таблица с данными</param>
        /// <param name="selectedRowIndex">Номер выбранной строки</param>
        /// <returns>Объект модели, полученный из строки таблицы</returns>
        public static T Parse<T>(this DataGridView dataGrid, int selectedRowIndex)
        {
            var values = new object[dataGrid.Columns.Count];
            var types = new Type[dataGrid.Columns.Count];

            var properties = typeof (T).GetProperties();
            for(int i=0;i<properties.Length;i++)
            {
                var attr = properties[i].GetCustomAttributes(false)
                    .ToDictionary(a => a.GetType().Name, a => a as DisplayAttribute);
                types[i] = attr["DisplayAttribute"].Type;
            }

            for (int i = 0; i < dataGrid.Columns.Count; i++)
            {
                //if (types[i] == typeof(Color))
                //{
                //    values[i] = ColorTranslator.FromHtml(dataGrid.Rows[selectedRowIndex].Cells[i].Value.ToString());
                //    continue;
                //}
                ///// TODO: for IEnumerable
                
                values[i] = dataGrid.Rows[selectedRowIndex].Cells[i].Value.ToString();
                types[i] = dataGrid.Rows[selectedRowIndex].Cells[i].Value.ToString().GetType();
            }

            /// TODO: solve problem with only object type in DataGridView cells
            var constr = typeof (T).GetConstructor(types);
            var parsedObject = (T) constr.Invoke(values);

            return parsedObject;
        }

        #endregion

        /// <summary>
        /// Определяет, имеется ли в строке таблицы пустые ячейки
        /// </summary>
        /// <param name="row">Строка таблицы</param>
        /// <returns>true, если в строке таблицы есть пустые ячейки</returns>
        public static bool HasEmptyCells(this DataGridViewRow row)
        {
            return row.Cells.Cast<object>().Any(cell => cell == null || cell.ToString().Length == 0);
        }
    }
}