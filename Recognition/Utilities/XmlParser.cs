﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace Recognition.Utilities
{
    public class XmlParser
    {
        /// <summary>
        /// Хранит корневой элемент XML-документа
        /// </summary>
        private XElement _stars;
        
        /// <summary>
        /// Создает новый экземпляр типа <see cref="XmlParser"/>
        /// </summary>
        /// <param name="filePath">Путь к файлу</param>
        public XmlParser(string filePath)
        {
            _stars = XElement.Load(filePath);
        }

        /// <summary>
        /// Получает все дочерние узлы для узла, который удовлетворяет селектору
        /// </summary>
        /// <typeparam name="T">Тип модели</typeparam>
        /// <param name="name">Селектор узла</param>
        /// <returns>Перечисление всех дочерних узлов, приведенных к типу модели</returns>
        public IEnumerable<T> ReadChilds<T>(string name)
        {
            var nodes = _stars.Element(name).Descendants();

            List<T> res = new List<T>(nodes.Count());
            foreach (XElement element in nodes)
            {
                var constr = typeof (T).GetConstructor(new Type[] {element.GetType()});
                var typed = (T) constr.Invoke(new object[] {element});
                res.Add(typed);
            }

            return res;
        }
    }
}