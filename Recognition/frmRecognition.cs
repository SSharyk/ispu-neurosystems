﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Recognition.Enums;
using Recognition.Models;
using Recognition.Solver;
using Recognition.Utilities;

namespace Recognition
{
    public partial class frmRecognition : Form
    {
        #region Поля

        /// <summary>
        /// Тип оценки эталонных образов
        /// </summary>
        private AssessmentType _assessmentType;

        #region Количественная оценка

        /// <summary>
        /// Перечисление эталонных образов для количественной оценки
        /// </summary>
        private IEnumerable<Star> _etalonsQuantity;

        /// <summary>
        /// Входной образ для количественной оценки
        /// </summary>
        private Star _inputQuantity;

        /// <summary>
        /// Перечисление результатов для количественной оценки
        /// </summary>
        private List<ResultViewQuantity> _outputsQuantity;

        #endregion

        #region Качественная оценка

        /// <summary>
        /// Перечисление эталонных образов для качественной оценки
        /// </summary>
        private IEnumerable<Substance> _etalonsQuality;

        /// <summary>
        /// Входной образ для качественной оценки
        /// </summary>
        private Substance _inputQuality;

        /// <summary>
        /// Перечисление результатов для качественной оценки
        /// </summary>
        private List<ResultViewQuality> _outputsQuality;

        #endregion

        #endregion

        #region Подготовка UI

        /// <summary>
        /// Конструктор формы
        /// </summary>
        public frmRecognition()
        {
            InitializeComponent();
            DisplayAssessmentTypes();

            //UpdateInputOutput();
        }

        /// <summary>
        /// Вызывается при переключении типа оценки
        /// </summary>
        protected void UpdateInputOutput()
        {
            try
            {
                ParseInputData();
                if (_assessmentType == AssessmentType.Количественная)
                {
                    tableEtalon.GenerateTable(_etalonsQuantity);
                    tableInput.GenerateTable(_inputQuantity);
                }
                else
                {
                    tableEtalon.GenerateTable(_etalonsQuality);
                    tableInput.GenerateTable(_inputQuality);
                }
                Log("Input table generated successfully");
            }
            catch (Exception e)
            {
                Log(e.Message);
            }

            Log("Ready to calculate");
            try
            {
                if (_assessmentType == AssessmentType.Количественная)
                {
                    SolveQuantity();
                    Log("Calculated successfully");
                    tableOutput.GenerateTable(_outputsQuantity.AsEnumerable());
                    Log("Output table generated successfully");
                }
                else
                {
                    SolveQuality();
                    Log("Calculated successfully");
                    tableOutput.GenerateTable(_outputsQuality.AsEnumerable());
                    Log("Output table generated successfully");
                }
            }
            catch (Exception e)
            {
                Log(e.Message);
            }
        }

        /// <summary>
        /// Выполнет чтение входных данных и преобразование их а объекты моделей
        /// </summary>
        protected void ParseInputData()
        {
            if (_assessmentType == AssessmentType.Количественная)
            {
                XmlParser parser = new XmlParser("../../InputData/EtalonsQuantity.xml");

                _etalonsQuantity = parser.ReadChilds<Star>("star-classes");
                Log("Etalons parsed successfully");

                _inputQuantity = parser.ReadChilds<Star>("star-input").FirstOrDefault();
                Log("Input parsed successfully");
            }
            else
            {
                XmlParser parser = new XmlParser("../../InputData/EtalonsQuality.xml");

                _etalonsQuality = parser.ReadChilds<Substance>("substance-classes");
                Log("Etalons parsed successfully");

                _inputQuality = parser.ReadChilds<Substance>("substance-input").FirstOrDefault();
                Log("Input parsed successfully");
            }
        }

        #endregion


        #region Решение задач распознания образов

        /// <summary>
        /// Выполняет решение задачи распознания образа с количественной оценкой
        /// </summary>
        protected void SolveQuantity()
        {
            this._outputsQuantity = new List<ResultViewQuantity>();
            var metrics = Enum.GetNames(typeof (DistanceType));
            List<double> radiuses = DomainCalculator.GetRadiuses(_etalonsQuantity);
            List<double> angles = DomainCalculator.GetAngles(_etalonsQuantity);
            double[,] scalars = DomainCalculator.CalculateAllScalars(_etalonsQuantity.ToList());
            double[] weights = DomainCalculator.CalculateAllWeights(_etalonsQuantity.ToList());
            foreach (string metric in metrics)
            {
                DistanceType metr = (DistanceType) Enum.Parse(typeof (DistanceType), metric);
                ResultViewQuantity res = new ResultViewQuantity(metr);
                double min = double.MaxValue;
                double max = double.MinValue;

                for (int i = 0; i < _etalonsQuantity.Count(); i++)
                {
                    try
                    {
                        double dist = MetricsCalculator.Solve(_inputQuantity, _etalonsQuantity.ElementAt(i), metr);
                        res.Distances.Add(dist);
                        if (metr == DistanceType.МоеСкалярноеПриближение)
                        {
                            res.DefinedClass = _inputQuantity.Class;
                            continue;
                        }
                        if ((dist < min && metr != DistanceType.СкалярноеПроизведение) ||
                            (dist > max && metr == DistanceType.СкалярноеПроизведение))
                        {
                            min = dist;
                            max = dist;
                            res.DefinedClass = _etalonsQuantity.ElementAt(i).Class;
                        }
                    }
                    catch (Exception e)
                    {
                        Log(e.Message);
                    }
                }

                _outputsQuantity.Add(res);
            }
        }

        /// <summary>
        /// Выполняет решение задачи распознания образа с количественной оценкой
        /// </summary>
        protected void SolveQuality()
        {
            this._outputsQuality = new List<ResultViewQuality>();
            var similarity = Enum.GetValues(typeof (SimilarityFunction));
            foreach (var funcObj in similarity)
            {
                SimilarityFunction func = (SimilarityFunction) funcObj;
                ResultViewQuality res = new ResultViewQuality(func);
                double max = double.MinValue;
                double min = double.MaxValue;

                for (int i = 0; i < _etalonsQuality.Count(); i++)
                {
                    SimilarityCalculator.CalculateParameters(_inputQuality, _etalonsQuality.ElementAt(i));
                    try
                    {
                        double dist = SimilarityCalculator.Solve(_inputQuality, _etalonsQuality.ElementAt(i), func);
                        res.Similarity.Add(dist);
                        if ((dist > max && func != SimilarityFunction.Хэмминг) ||
                            (dist < min && func == SimilarityFunction.Хэмминг))
                        {
                            max = dist;
                            min = dist;
                            res.DefinedClass =
                                (SubstanceClass) Enum.Parse(typeof (SubstanceClass), _etalonsQuality.ElementAt(i).Class);
                        }
                    }
                    catch (Exception e)
                    {
                        Log(e.Message);
                    }
                }

                _outputsQuality.Add(res);
            }
        }

        #endregion


        #region Обработчики событий UI; вспомогательные методы

        /// <summary>
        /// Открывает окно лога
        /// </summary>
        /// <param name="sender">Объект-инициатор события</param>
        /// <param name="e">Данные события</param>
        private void cmdShowLog_Click(object sender, EventArgs e)
        {
            if (cmdShowLog.Text == ">")
            {
                this.Width += (txtLog.Width + 10);
                cmdShowLog.Text = "<";
            }
            else
            {
                this.Width -= (txtLog.Width + 10);
                cmdShowLog.Text = ">";
            }
        }

        /// <summary>
        /// Вызывается, когда пользователь редактирует ячейку таблицы
        /// </summary>
        /// <param name="sender">Объект-инициатор события</param>
        /// <param name="e">Данные события</param>
        private void InputEdited(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!(sender as DataGridView).Rows[e.RowIndex].IsNewRow &&
                    !(sender as DataGridView).Rows[e.RowIndex].HasEmptyCells())
                {
                    if (_assessmentType == AssessmentType.Количественная)
                    {
                        _inputQuantity = tableInput.Parse<Star>(e.RowIndex);
                        this.SolveQuantity();
                        tableOutput.GenerateTable(_outputsQuantity.AsEnumerable());
                    }
                    else
                    {
                        _inputQuality = tableInput.Parse<Substance>(e.RowIndex);
                        this.SolveQuality();
                        tableOutput.GenerateTable(_outputsQuality.AsEnumerable());
                    }
                }
                Log("Input was changed successfully");
            }
            catch (Exception exc)
            {
                Log(exc.Message);
            }
        }

        /// <summary>
        /// Вызывается, когда пользователь изменяет тип оценки
        /// </summary>
        /// <param name="sender">Объект-инициатор события</param>
        /// <param name="e">Данные события</param>
        private void lstAssessments_SelectedIndexChanged(object sender, EventArgs e)
        {
            _assessmentType = (AssessmentType) (sender as ComboBox).SelectedIndex;
            UpdateInputOutput();
        }

        /// <summary>
        /// Отображает список типов оценки
        /// </summary>
        protected void DisplayAssessmentTypes()
        {
            this.lstAssessments.Items.AddRange(Enum.GetNames(typeof (AssessmentType)));
            lstAssessments.SelectedIndex = 0;
            _assessmentType = AssessmentType.Количественная;
        }

        /// <summary>
        /// Добавляет запись в лог
        /// </summary>
        /// <param name="message">Сообщение</param>
        public virtual void Log(string message)
        {
            txtLog.Text += string.Format("{0}: {1}{2}{2}", DateTime.Now, message, "\r\n");
        }

        #endregion

    }
}