﻿namespace Recognition.Enums
{
    /// <summary>
    /// Задает способ расчета метрики при качественной оценке
    /// </summary>
    public enum SimilarityFunction
    {
        S1,
        S2,
        S3,
        S4,
        S5,
        S6,
        S7,

        Sм,

        Хэмминг
    }
}