﻿namespace Recognition.Enums
{
    /// <summary>
    /// Задает способ оценки эталонных образов
    /// </summary>
    public enum AssessmentType
    {
        Количественная,
        Качественная
    }
}