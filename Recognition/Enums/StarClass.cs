﻿namespace Recognition.Enums
{
    /// <summary>
    /// Задает спектральный класс звезды
    /// </summary>
    public enum StarClass
    {
        O = 1,
        B,
        A,
        F,
        G,
        K,
        M,

        No = 0
    }
}