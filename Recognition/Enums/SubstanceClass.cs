﻿namespace Recognition.Enums
{
    public enum SubstanceClass
    {
        No,
        Металл,
        КислотныйОксид,
        Щелочь,
        Кислота,
        Соль
    }
}