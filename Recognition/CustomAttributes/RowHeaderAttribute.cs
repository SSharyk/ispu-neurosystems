﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recognition.CustomAttributes
{
    /// <summary>
    /// Атрибут для указания факта, что содержимое столбца должно отображаться слева от остального содержимого строки
    /// </summary>
    public class RowHeaderAttribute : Attribute
    {
    }
}