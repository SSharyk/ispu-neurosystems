﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recognition.CustomAttributes
{
    /// <summary>
    /// Атрибут для задания свойств столбцов таблиц
    /// </summary>
    class DisplayAttribute:Attribute
    {
        /// <summary>
        /// Получает или задает заголовок столбца
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Получает или задает атрибут узла XML-документа, который описывает эталонный образ
        /// </summary>
        public string Attribute { get; set; }

        /// <summary>
        /// Получает или задает тип данных, который хранится в ячейках столбца
        /// </summary>
        public Type Type { get; set; }
    }
}
