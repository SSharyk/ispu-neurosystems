﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Recognition.Models;
using Recognition.Enums;

namespace Recognition.Solver
{
    /// <summary>
    /// Выполняет распознание образов при качественной оценке
    /// </summary>
    public static class SimilarityCalculator
    {
        private static double _a;
        private static double _b;
        private static double _g;
        private static double _h;
        private static double _n
        {
            get { return _a + _b + _g + _h; }
        }
        /// <summary>
        /// Расстояние Хэмминга
        /// </summary>
        private static double _hamming
        {
            get { return _g + _h; }
        }

        /// <summary>
        /// Набор функций сходства
        /// </summary>
        private static Func<double>[] _similarity = new Func<double>[]
            {
                () => _a/_n,
                () => _a/(_n - _b),
                () => _a/(2*_a + _g + _h),
                () => _a/(_a + 2*_g + 2*_h),
                () => (_a + _b)/_n,
                () => _a/(_g + _h),
                () => (_a*_b - _g*_h)/(_a*_b + _g*_h)
            };

        /// TODO: implement "Singletone" pattern
        /// <summary>
        /// Вычисляет параметры, характеризующие общность/различие образов
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        public static void CalculateParameters(Substance image1, Substance image2)
        {
            _a = _b = _g = _h = 0;
            var properties = typeof (Substance).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.PropertyType == typeof (bool))
                {
                    var xik = (bool) property.GetValue(image1, null);
                    var xjk = (bool) property.GetValue(image2, null);
                    _a += (xik && xjk) ? 1 : 0;
                    _b += (!xik && xjk) ? 1 : 0;
                    _g += (xik && !xjk) ? 1 : 0;
                    _h += (!xik && xjk) ? 1 : 0;
                }
            }
        }

        /// <summary>
        /// Основной интерфейсный метод расчета расстояний между образами при качественной оценке
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <param name="func">Способ сравнения</param>
        /// <returns>Вещественное число - степень сходства образов</returns>
        public static double Solve(Substance image1, Substance image2, SimilarityFunction func)
        {
            switch (func)
            {
                case SimilarityFunction.S1:
                case SimilarityFunction.S2:
                case SimilarityFunction.S3:
                case SimilarityFunction.S4:
                case SimilarityFunction.S5:
                case SimilarityFunction.S6:
                case SimilarityFunction.S7:
                    {
                        int number = func.ToString()[1] - '0' - 1;
                        return _similarity[number].Invoke();
                    }
                
                case SimilarityFunction.Sм:
                    return Math.Abs((_a + _b) - (_g + _h))/_n;
                
                case SimilarityFunction.Хэмминг:
                    return _hamming;

                default:
                    throw new ArgumentOutOfRangeException("func");
            }
        }
    }
}
