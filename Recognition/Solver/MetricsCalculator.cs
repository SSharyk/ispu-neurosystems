﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Recognition.Enums;
using Recognition.Models;

namespace Recognition.Solver
{
    /// <summary>
    /// Выполняет распознание образов при количественной оценке
    /// </summary>
    public static class MetricsCalculator
    {
        /// <summary>
        /// Вычисляет расстояние между образами по Евклиду
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Расстояние между образами по Евклиду</returns>
        public static double Euclid(Star image1, Star image2)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double)modelProperty.GetValue(image1, null);
                    var val2 = (double)modelProperty.GetValue(image2, null);
                    double s = (val1 - val2) * (val1 - val2);
                    metrics += s;
                }
            }
            metrics = Math.Sqrt(metrics);
            return metrics;
        }

        /// <summary>
        /// Вычисляет расстояние между образами по Минковскому
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <param name="lambda">Показатель степени</param>
        /// <returns>Расстояние между образами по Минковскому</returns>
        public static double Minkowski(Star image1, Star image2, int lambda)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double)modelProperty.GetValue(image1, null);
                    var val2 = (double)modelProperty.GetValue(image2, null);
                    double s = Math.Pow((val1 - val2),lambda);
                    metrics += s;
                }
            }
            metrics = Math.Pow(Math.Abs(metrics), 1.0/lambda);
            return metrics;
        }

        /// <summary>
        /// Вычисляет расстояние между образами по модулям разности
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Расстояние между образами по модулям разности</returns>
        public static double Manhattan(Star image1, Star image2)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double)modelProperty.GetValue(image1, null);
                    var val2 = (double)modelProperty.GetValue(image2, null);
                    double s = Math.Abs(val1 - val2);
                    metrics += s;
                }
            }            
            return metrics;
        }

        /// <summary>
        /// Вычисляет расстояние между образами по Канберру
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Расстояние между образами по Канберру</returns>
        public static double Canberra(Star image1, Star image2)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double)modelProperty.GetValue(image1, null);
                    var val2 = (double)modelProperty.GetValue(image2, null);
                    double s = Math.Abs(val1 - val2) / Math.Abs(val1 + val2);
                    metrics += s;
                }
            }
            return metrics;
        }

        /// <summary>
        /// Вычисляет угол между векторами образов
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Угол между векторами образов</returns>
        public static double VectorsAngle(Star image1, Star image2)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            double length1 = 0, length2 = 0;
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double) modelProperty.GetValue(image1, null);
                    var val2 = (double) modelProperty.GetValue(image2, null);
                    double s = val1*val2;
                    length1 += (val1*val1);
                    length2 += (val2*val2);
                    metrics += s;
                }
            }
            metrics = metrics/(Math.Sqrt(length1*length2));
            return Math.Acos(metrics);
        }

        /// <summary>
        /// Вычисляет скалярное произведение векторов образов
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Скалярное произведение векторов образов</returns>
        public static double VectorsScalarProduct(Star image1, Star image2)
        {
            double metrics = 0.0;
            var modelProperties = image1.GetType().GetProperties();
            foreach (PropertyInfo modelProperty in modelProperties)
            {
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double) modelProperty.GetValue(image1, null);
                    var val2 = (double) modelProperty.GetValue(image2, null);
                    metrics += (val1*val2);
                }
            }
            return metrics;
        }

        /// <summary>
        /// Вычисляет скалярное произведение векторов образов с учетом весовых коэффициентов
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Скалярное произведение векторов образов</returns>
        public static double VectorsWeightScalarProduct(Star image1, Star image2)
        {
            double metrics = 0.0;
            var weights = DomainCalculator.Weights;
            var modelProperties = image1.GetType().GetProperties();
            for (int i = 0; i < modelProperties.Length; i++)
            {
                PropertyInfo modelProperty = modelProperties[i];
                if (modelProperty.PropertyType.Name == "Double")
                {
                    var val1 = (double) modelProperty.GetValue(image1, null);
                    var val2 = (double) modelProperty.GetValue(image2, null);
                    metrics += (val1*val2*weights[i]*weights[i]);
                }
            }
            return metrics;
        }

        /// <summary>
        /// Вычисляет скалярное произведение векторов образов и учитывает соотношения между эталонами
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Скалярное произведение векторов образов</returns>
        public static double VectorScalarApproximation(Star image1, Star image2)
        {
            double metrics = VectorsScalarProduct(image1, image2);
            var approx = DomainCalculator.Scalars;
            int etalonNumber = (int) image2.Class - 1;
            int index = 0;
            for (var i = 0; i < approx.GetLength(1); i++)
            {
                if (Math.Abs(approx[etalonNumber, i] - metrics) < 
                    Math.Abs(approx[etalonNumber, index] - metrics))
                {
                    index = i;
                }
            }
            image1.Class = (StarClass) (index + 1);
            return Math.Abs(approx[etalonNumber, index] - approx[etalonNumber, etalonNumber]);
        }

        /// <summary>
        /// Вычисляет расстояние между векторами с использованием непересекающихся сфер
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Расстояние между векторами, вычисленное с использованием непересекающихся сфер</returns>
        public static double VectorSpheres(Star image1, Star image2)
        {
            var allowedRadiuses = DomainCalculator.Radiuses;
            double dist = Euclid(image1, image2);
            int index = 0;
            for (int i = 0; i < allowedRadiuses.Count; i++)
            {
                if (dist < allowedRadiuses[i] && allowedRadiuses[i] < allowedRadiuses[index])
                    index = i;
            }
            return dist;
        }

        /// <summary>
        /// Вычисляет расстояние между векторами с использованием непересекающихся конусов
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <returns>Расстояние между векторами, вычисленное с использованием непересекающихся конусов</returns>
        public static double VectorCones(Star image1, Star image2)
        {
            var allowedAngles = DomainCalculator.Angles;
            double angle = VectorsAngle(image1, image2);
            int index = 0;
            for (int i = 0; i < allowedAngles.Count; i++)
            {
                if (angle < allowedAngles[i] &&
                    Math.Abs(angle - allowedAngles[i]) < Math.Abs(angle - allowedAngles[index]))
                    index = i;
            }
            return angle;
        }

        /// <summary>
        /// Основной интерфейсный метод расчета расстояний между образами
        /// </summary>
        /// <param name="image1">Первый образ для сравнения</param>
        /// <param name="image2">Второй образ для сравнения</param>
        /// <param name="distanceType">Тип метрики</param>
        /// <returns>Расстояние между образами, вычисленное с использованием заданной метрики</returns>
        public static double Solve(Star image1, Star image2, DistanceType distanceType)
        {
            switch (distanceType)
            {
                case DistanceType.Евклид:
                    return Euclid(image1, image2);
                case DistanceType.Минковский:
                    return Minkowski(image1, image2, 3);
                case DistanceType.Манхэттан:
                    return Manhattan(image1, image2);
                case DistanceType.Канберра:
                    return Canberra(image1, image2);
                case DistanceType.УголМеждуВекторами:
                    return VectorsAngle(image1, image2);
                case DistanceType.СкалярноеПроизведение:
                    return VectorsScalarProduct(image1, image2);
                case DistanceType.МоеСкалярноеПриближение:
                    return VectorScalarApproximation(image1, image2);
                case DistanceType.ВзвешенноеСкалярноеПроизведение:
                    return VectorsWeightScalarProduct(image1, image2);
                case DistanceType.ШарообразныеОбласти:
                    return VectorSpheres(image1, image2);
                case DistanceType.КонусообразныеОбласти:
                    return VectorCones(image1, image2);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
