﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Recognition.Models;

namespace Recognition.Solver
{
    /// <summary>
    /// Выполняет расчеты, связанные с неперсекающимися областями в пространстве
    /// </summary>
    internal class DomainCalculator
    {
        #region Сферы

        /// <summary>
        /// Получает или задает радиусы непересекающихся сфер
        /// </summary>
        public static List<double> Radiuses { get; protected set; }

        /// <summary>
        /// Попарно вычисляет расстояние по Евклиду между всеми эталонами
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Квадратный массив со значениями метрик</returns>
        private static double[,] CalculateAllRadiuses(List<Star> input)
        {
            var distances = new double[input.Count,input.Count];
            for (var i = 0; i < input.Count - 1; i++)
            {
                for (var j = i + 1; j < input.Count; j++)
                {
                    distances[i, j] = distances[j, i] = MetricsCalculator.Euclid(input[i], input[j]);
                }
            }

            return distances;
        }

        /// <summary>
        /// Определяет макисмально допустимые радиусы непересекающихся сфер
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Список радиусов сфер</returns>
        public static List<double> GetRadiuses(IEnumerable<Star> input)
        {
            var distances = CalculateAllRadiuses(input.ToList());
            Radiuses = Enumerable.Repeat(double.MaxValue, input.Count()).ToList();
            for (int i = 0; i < distances.GetLength(0); i++)
            {
                for (int j = 0; j < distances.GetLength(1); j++)
                {
                    if (distances[i, j] > 0.01 && distances[i, j] < Radiuses[i]) Radiuses[i] = distances[i, j];
                }
            }
            //Radiuses = distances.Keys.Select(dist => distances[dist].Min()).ToList();
            return Radiuses;
        }

        #endregion

        #region Конусы

        /// <summary>
        /// Получает или задает радиусы непересекающихся сфер
        /// </summary>
        public static List<double> Angles { get; protected set; }

        /// <summary>
        /// Попарно вычисляет расстояние по Евклиду между всеми эталонами
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Квадратный массив со значениями метрик</returns>
        private static double[,] CalculateAllAngles(List<Star> input)
        {
            var angles = new double[input.Count, input.Count];
            for (var i = 0; i < input.Count; i++)
            {
                for (var j = 0; j < input.Count; j++)
                {
                    angles[i, j] = MetricsCalculator.VectorsAngle(input[i], input[j]);
                    //angles[j, i] = MetricsCalculator.VectorsAngle(input[j], input[i]); 
                }
            }

            return angles;
        }

        /// <summary>
        /// Определяет макисмально допустимые радиусы непересекающихся сфер
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Список радиусов сфер</returns>
        public static List<double> GetAngles(IEnumerable<Star> input)
        {
            var angles = CalculateAllAngles(input.ToList());
            Angles = Enumerable.Repeat(double.MaxValue, input.Count()).ToList();
            for (int i = 0; i < angles.GetLength(0); i++)
            {
                for (int j = 0; j < angles.GetLength(1); j++)
                {
                    if (Math.Abs(angles[i, j]) > 0.01 && Math.Abs(angles[i, j]) < Math.Abs(Angles[i])) 
                        Angles[i] = angles[i, j];
                }
            }
            //Radiuses = distances.Keys.Select(dist => distances[dist].Min()).ToList();
            return Angles;
        }

        #endregion
        
        #region Скалярное приближение

        /// <summary>
        /// Получает или задает значения скалярного произведения между эталонами
        /// </summary>
        public static double[,] Scalars { get; protected set; }

        /// <summary>
        /// Попарно вычисляет векторное произведение между всеми эталонами
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Квадратный массив со значениями метрик</returns>
        public static double[,] CalculateAllScalars(List<Star> input)
        {
            Scalars = new double[input.Count, input.Count];
            for (var i = 0; i < input.Count; i++)
            {
                for (var j = 0; j < input.Count; j++)
                {
                    Scalars[i, j] = MetricsCalculator.VectorsScalarProduct(input[i], input[j]);
                }
            }
            return Scalars;
        }

        /// <summary>
        /// Получает или задает веса для скалярного произведения
        /// </summary>
        public static double[] Weights { get; protected set; }

        /// <summary>
        /// Попарно вычисляет веса для скалярного произведение
        /// </summary>
        /// <param name="input">Список исходных данных (эталоны)</param>
        /// <returns>Квадратный массив со значениями метрик</returns>
        public static double[] CalculateAllWeights(List<Star> input)
        {
            var modelProperties = input.ElementAt(0).GetType().GetProperties();
            Weights = new double[modelProperties.Length];
            for (int j = 1; j < input.Count; j++)
            {
                for (var i = 0; i < modelProperties.Length; i++)
                {
                    PropertyInfo modelProperty = modelProperties[i];
                    if (modelProperty.PropertyType.Name == "Double")
                    {
                        var val1 = (double) modelProperty.GetValue(input.First(), null);
                        var val2 = (double) modelProperty.GetValue(input.ElementAt(j), null);
                        Weights[i] += (Math.Min(val2/val1, val1/val2));
                    }
                }
            }
            return Weights.Select(x => x/(double) input.Count).ToArray();
        }

        #endregion    
    }
}