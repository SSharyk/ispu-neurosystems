﻿namespace Recognition
{
    partial class frmRecognition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpInput = new System.Windows.Forms.GroupBox();
            this.tableInput = new System.Windows.Forms.DataGridView();
            this.tableEtalon = new System.Windows.Forms.DataGridView();
            this.grpOutput = new System.Windows.Forms.GroupBox();
            this.tableOutput = new System.Windows.Forms.DataGridView();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.cmdShowLog = new System.Windows.Forms.Button();
            this.lblCalcType = new System.Windows.Forms.Label();
            this.lstAssessments = new System.Windows.Forms.ComboBox();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.grpInput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEtalon)).BeginInit();
            this.grpOutput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableOutput)).BeginInit();
            this.SuspendLayout();
            // 
            // grpInput
            // 
            this.grpInput.Controls.Add(this.tableInput);
            this.grpInput.Controls.Add(this.tableEtalon);
            this.grpInput.Location = new System.Drawing.Point(13, 41);
            this.grpInput.Name = "grpInput";
            this.grpInput.Size = new System.Drawing.Size(626, 342);
            this.grpInput.TabIndex = 0;
            this.grpInput.TabStop = false;
            this.grpInput.Text = "Входные данные";
            // 
            // tableInput
            // 
            this.tableInput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableInput.Location = new System.Drawing.Point(3, 228);
            this.tableInput.Name = "tableInput";
            this.tableInput.Size = new System.Drawing.Size(620, 111);
            this.tableInput.TabIndex = 1;
            this.tableInput.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.InputEdited);
            // 
            // tableEtalon
            // 
            this.tableEtalon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableEtalon.Location = new System.Drawing.Point(3, 19);
            this.tableEtalon.Name = "tableEtalon";
            this.tableEtalon.ReadOnly = true;
            this.tableEtalon.Size = new System.Drawing.Size(620, 203);
            this.tableEtalon.TabIndex = 0;
            // 
            // grpOutput
            // 
            this.grpOutput.Controls.Add(this.tableOutput);
            this.grpOutput.Location = new System.Drawing.Point(645, 41);
            this.grpOutput.Name = "grpOutput";
            this.grpOutput.Size = new System.Drawing.Size(408, 342);
            this.grpOutput.TabIndex = 1;
            this.grpOutput.TabStop = false;
            this.grpOutput.Text = "Результаты вычислений";
            // 
            // tableOutput
            // 
            this.tableOutput.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableOutput.Location = new System.Drawing.Point(3, 16);
            this.tableOutput.Name = "tableOutput";
            this.tableOutput.ReadOnly = true;
            this.tableOutput.Size = new System.Drawing.Size(402, 323);
            this.tableOutput.TabIndex = 0;
            // 
            // txtLog
            // 
            this.txtLog.Location = new System.Drawing.Point(1083, 41);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(110, 342);
            this.txtLog.TabIndex = 2;
            // 
            // cmdShowLog
            // 
            this.cmdShowLog.Location = new System.Drawing.Point(1059, 41);
            this.cmdShowLog.Name = "cmdShowLog";
            this.cmdShowLog.Size = new System.Drawing.Size(18, 342);
            this.cmdShowLog.TabIndex = 3;
            this.cmdShowLog.Text = ">";
            this.cmdShowLog.UseVisualStyleBackColor = true;
            this.cmdShowLog.Click += new System.EventHandler(this.cmdShowLog_Click);
            // 
            // lblCalcType
            // 
            this.lblCalcType.AutoSize = true;
            this.lblCalcType.Location = new System.Drawing.Point(10, 13);
            this.lblCalcType.Name = "lblCalcType";
            this.lblCalcType.Size = new System.Drawing.Size(118, 13);
            this.lblCalcType.TabIndex = 2;
            this.lblCalcType.Text = "Проводить оценку по ";
            // 
            // lstAssessments
            // 
            this.lstAssessments.FormattingEnabled = true;
            this.lstAssessments.Location = new System.Drawing.Point(134, 10);
            this.lstAssessments.Name = "lstAssessments";
            this.lstAssessments.Size = new System.Drawing.Size(237, 21);
            this.lstAssessments.TabIndex = 3;
            this.lstAssessments.SelectedIndexChanged += new System.EventHandler(this.lstAssessments_SelectedIndexChanged);
            // 
            // lblCopyright
            // 
            this.lblCopyright.AutoSize = true;
            this.lblCopyright.Location = new System.Drawing.Point(950, 10);
            this.lblCopyright.Name = "lblCopyright";
            this.lblCopyright.Size = new System.Drawing.Size(122, 13);
            this.lblCopyright.TabIndex = 4;
            this.lblCopyright.Text = "Sharykin S., ISPU. 2016";
            // 
            // frmRecognition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 386);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lstAssessments);
            this.Controls.Add(this.cmdShowLog);
            this.Controls.Add(this.lblCalcType);
            this.Controls.Add(this.txtLog);
            this.Controls.Add(this.grpOutput);
            this.Controls.Add(this.grpInput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmRecognition";
            this.Text = "Распознание образов";
            this.grpInput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableEtalon)).EndInit();
            this.grpOutput.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tableOutput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpInput;
        private System.Windows.Forms.GroupBox grpOutput;
        private System.Windows.Forms.DataGridView tableEtalon;
        private System.Windows.Forms.DataGridView tableInput;
        private System.Windows.Forms.DataGridView tableOutput;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button cmdShowLog;
        private System.Windows.Forms.Label lblCalcType;
        private System.Windows.Forms.ComboBox lstAssessments;
        private System.Windows.Forms.Label lblCopyright;
    }
}