﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Neuronets
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Перобразует двумерный массив в одномерный
        /// </summary>
        /// <param name="et"> 2м массив </param>
        /// <returns> 1м массив </returns>
        public static double[] ToOneDimensionArray(this double[,] et)
        {
            double[] et1 = new double[et.Length];
            int k = -1;
            for (int i = 0; i < et.GetLength(0); i++)
            {
                for (int j = 0; j < et.GetLength(1); j++)
                {
                    et1[++k] = et[i, j];
                }
            }
            return et1;
        }

        public static double[,] ToTwoDimensionArrayByWidth(this double[] array, int width)
        {
            /// TODO: test if size is not compatible with specific width|height
            /// (e.g. vector with length=35 convert to matrix with width=4)
            int height = array.Length/width;
            double[,] two = new double[height,width];
            for (int k = 0; k < array.Length; k++)
            {
                try
                {
                    int i = k/width;
                    int j = k%width;
                    two[i, j] = array[k];
                }
                catch{}
            }
            return two;
        }

        public static double[,] ToTwoDimensionArrayByHeight(this double[] array, int height)
        {
            /// TODO: test if size is not compatible with specific width|height
            /// (e.g. vector with length=35 convert to matrix with width=4)
            int width = array.Length / height;
            double[,] two = new double[height, width];
            for (int k = 0; k < array.Length; k++)
            {
                try
                {
                    int i = k / width;
                    int j = k % width;
                    two[i, j] = array[k];
                }
                catch { }
            }
            return two;
        }

        public static double[] Copy(this double[] array)
        {
            double[] res = new double[array.Length];
            for (int i = 0; i < array.Length; i++)
                res[i] = array[i];
            return res;
        }

        #region add line/column

        public static double[,] AddColumn(this double[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);
            double[,] newArray = new double[height,width + 1];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    newArray[i, j] = array[i, j];
                }
                newArray[i, width] = -1;
            }
            return newArray;
        }

        public static double[] AddColumn(this double[] array, int targetWidth)
        {
            var two = array.ToTwoDimensionArrayByWidth(targetWidth);
            return two.AddColumn().ToOneDimensionArray();
        }
        
        public static double[,] AddRow(this double[,] array)
        {
            double[,] arrNew = new double[array.GetLength(0)+1, array.GetLength(1)];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    arrNew[i, j] = array[i, j];
                }
            }
            for (int j = 0; j < array.GetLength(1); j++)
            {
                arrNew[arrNew.GetLength(0) - 1, j] = -1;
            }
            return arrNew;
        }

        public static double[] AddRow(this double[] array, int rowLength)
        {
            double[] arrNew = new double[array.Length+rowLength];
            for (int i = 0; i < array.Length; i++)
            {
                arrNew[i] = array[i];
            }
            for (int j = array.Length; j < arrNew.Length; j++)
            {
                arrNew[j] = -1;
            }
            return arrNew;
        }

        #endregion


        #region remove line/column

        public static double[,] RemoveColumn(this double[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);
            double[,] newArray = new double[height, width - 1];
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width-1; j++)
                {
                    newArray[i, j] = array[i, j];
                }
            }
            return newArray;
        }

        public static double[] RemoveColumn(this double[] array, int targetWidth)
        {
            var two = array.ToTwoDimensionArrayByWidth(targetWidth);
            return two.RemoveColumn().ToOneDimensionArray();
        }

        public static double[,] RemoveRow(this double[,] array)
        {
            int height = array.GetLength(0);
            int width = array.GetLength(1);
            double[,] newArray = new double[height - 1,width];
            for (int i = 0; i < height - 1; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    newArray[i, j] = array[i, j];
                }
            }
            return newArray;
        }

        public static double[] RemoveRow(this double[] array, int targetHeight)
        {
            var two = array.ToTwoDimensionArrayByHeight(targetHeight);
            return two.RemoveRow().ToOneDimensionArray();
        }

        #endregion
    }
}
