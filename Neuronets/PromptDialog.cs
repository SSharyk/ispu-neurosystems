﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Neuronets
{
    public static class PromptDialog
    {
        public static string ShowDialog(string caption, string windowTitle)
        {
            Form prompt = new Form()
                {
                    Width = 250,
                    Height = 150,
                    FormBorderStyle = FormBorderStyle.FixedDialog,
                    Text = windowTitle,
                    StartPosition = FormStartPosition.CenterScreen
                };

            Label textLabel = new Label() {Left = 50, Top = 20, Text = caption};
            TextBox textBox = new TextBox() {Left = 50, Top = 50, Width = 150};
            Button confirmation = new Button()
                {Text = "OK", Left = 50, Width = 70, Top = 80, DialogResult = DialogResult.OK};
            Button cancellation = new Button()
                {Text = "Cancel", Left = 130, Width = 70, Top = 80, DialogResult = DialogResult.Cancel};

            confirmation.Click += (sender, e) => { prompt.Close(); };
            cancellation.Click += (sender, e) => { prompt.Close(); };

            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(cancellation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;
            prompt.CancelButton = cancellation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }
}