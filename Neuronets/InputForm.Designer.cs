﻿namespace Neuronets
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numHeight = new System.Windows.Forms.NumericUpDown();
            this.numWidth = new System.Windows.Forms.NumericUpDown();
            this.cmdClear = new System.Windows.Forms.Button();
            this.cmdSaveImage = new System.Windows.Forms.Button();
            this.lstViewEtalons = new System.Windows.Forms.ComboBox();
            this.imgInput = new System.Windows.Forms.PictureBox();
            this.imgOutput = new System.Windows.Forms.PictureBox();
            this.cmdStart = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOutput)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numHeight);
            this.groupBox1.Controls.Add(this.numWidth);
            this.groupBox1.Controls.Add(this.cmdClear);
            this.groupBox1.Controls.Add(this.cmdSaveImage);
            this.groupBox1.Controls.Add(this.lstViewEtalons);
            this.groupBox1.Controls.Add(this.imgInput);
            this.groupBox1.Location = new System.Drawing.Point(11, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(217, 155);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исходные данные";
            // 
            // numHeight
            // 
            this.numHeight.Location = new System.Drawing.Point(146, 125);
            this.numHeight.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numHeight.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numHeight.Name = "numHeight";
            this.numHeight.Size = new System.Drawing.Size(63, 20);
            this.numHeight.TabIndex = 11;
            this.numHeight.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numHeight.Visible = false;
            this.numHeight.ValueChanged += new System.EventHandler(this.numHeight_ValueChanged);
            // 
            // numWidth
            // 
            this.numWidth.Location = new System.Drawing.Point(146, 105);
            this.numWidth.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numWidth.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numWidth.Name = "numWidth";
            this.numWidth.Size = new System.Drawing.Size(63, 20);
            this.numWidth.TabIndex = 10;
            this.numWidth.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numWidth.Visible = false;
            this.numWidth.ValueChanged += new System.EventHandler(this.numWidth_ValueChanged);
            // 
            // cmdClear
            // 
            this.cmdClear.Location = new System.Drawing.Point(147, 47);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(63, 23);
            this.cmdClear.TabIndex = 9;
            this.cmdClear.Text = "Clear";
            this.cmdClear.UseVisualStyleBackColor = true;
            this.cmdClear.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmdClear_MouseClick);
            // 
            // cmdSaveImage
            // 
            this.cmdSaveImage.Location = new System.Drawing.Point(147, 76);
            this.cmdSaveImage.Name = "cmdSaveImage";
            this.cmdSaveImage.Size = new System.Drawing.Size(63, 23);
            this.cmdSaveImage.TabIndex = 8;
            this.cmdSaveImage.Text = "Save";
            this.cmdSaveImage.UseVisualStyleBackColor = true;
            this.cmdSaveImage.Click += new System.EventHandler(this.cmdSaveImage_Click);
            // 
            // lstViewEtalons
            // 
            this.lstViewEtalons.FormattingEnabled = true;
            this.lstViewEtalons.Location = new System.Drawing.Point(146, 20);
            this.lstViewEtalons.Name = "lstViewEtalons";
            this.lstViewEtalons.Size = new System.Drawing.Size(64, 21);
            this.lstViewEtalons.TabIndex = 7;
            this.lstViewEtalons.SelectedIndexChanged += new System.EventHandler(this.lstViewEtalons_SelectedIndexChanged);
            // 
            // imgInput
            // 
            this.imgInput.BackColor = System.Drawing.Color.White;
            this.imgInput.Location = new System.Drawing.Point(20, 20);
            this.imgInput.Name = "imgInput";
            this.imgInput.Size = new System.Drawing.Size(120, 129);
            this.imgInput.TabIndex = 0;
            this.imgInput.TabStop = false;
            this.imgInput.MouseClick += new System.Windows.Forms.MouseEventHandler(this.imgInput_MouseClick);
            // 
            // imgOutput
            // 
            this.imgOutput.BackColor = System.Drawing.Color.White;
            this.imgOutput.Location = new System.Drawing.Point(31, 165);
            this.imgOutput.Name = "imgOutput";
            this.imgOutput.Size = new System.Drawing.Size(120, 118);
            this.imgOutput.TabIndex = 10;
            this.imgOutput.TabStop = false;
            // 
            // cmdStart
            // 
            this.cmdStart.Location = new System.Drawing.Point(157, 186);
            this.cmdStart.Name = "cmdStart";
            this.cmdStart.Size = new System.Drawing.Size(70, 70);
            this.cmdStart.TabIndex = 11;
            this.cmdStart.Text = "===>";
            this.cmdStart.UseVisualStyleBackColor = true;
            this.cmdStart.Click += new System.EventHandler(this.cmdStart_Click);
            // 
            // InputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(245, 299);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.imgOutput);
            this.Controls.Add(this.cmdStart);
            this.Name = "InputForm";
            this.Text = "Распознание инициалов";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgOutput)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox imgInput;
        private System.Windows.Forms.PictureBox imgOutput;
        private System.Windows.Forms.Button cmdStart;
        private System.Windows.Forms.Button cmdClear;
        private System.Windows.Forms.Button cmdSaveImage;
        private System.Windows.Forms.ComboBox lstViewEtalons;
        private System.Windows.Forms.NumericUpDown numHeight;
        private System.Windows.Forms.NumericUpDown numWidth;
    }
}

