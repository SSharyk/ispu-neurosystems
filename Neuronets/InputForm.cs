﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Neuronets
{
    public partial class InputForm : Form
    {
        #region fields and properties

        private const int _pixelsInSector = 4;  // число точек в сегменте (исключая угловые)
        private const int _pixelSize = 20;      // размер каждой точки
        private int ImageHeight = 5;
        private int ImageWidth = 5;

        private List<double[]> _etalons;    // эталонные образы
        private double[] _input;            // входной вектор
        private double[] _output;

        private double[] _weights;
        
        #endregion

        public InputForm()
        {
            InitializeComponent();

            _etalons = new List<double[]>();
            _etalons.Clear();
            lstViewEtalons.Items.Clear();

            toMoveControlsAfterRecising();
            toInitEtalons();
            _input = _etalons.ElementAt(0);
            lstViewEtalons.SelectedIndex = 0;
            toDrawSymbol(_input);
        }

        #region Эталоны

        private void toInitEtalons()
        {
            double[,] et = new double[ImageHeight, ImageWidth];

            string[] files = Directory.GetFiles("./../../Etalons/");
            foreach (string path in files)
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    StreamReader reader = new StreamReader(stream);
                    string line = reader.ReadLine();
                    double[] elements =
                        line.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(double.Parse).ToArray();
                    _etalons.Add(elements);
                    lstViewEtalons.Items.Add(path.Substring(path.LastIndexOf('/')+1));
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Заполнение массива
        /// </summary>
        /// <param name="et"> ref-массив </param>
        /// <param name="val"> значения элементов </param>       
        private void zero(ref double[,] et, double val)
        {
            for (int i = 0; i < et.GetLength(0); i++)
            {
                for (int j = 0; j < et.GetLength(1); j++)
                {
                    et[i, j] = val;
                }
            }
        }

        private void toSaveEtalonIntoFile(string title, double[] newEtalon, bool isTracked=false)
        {
            title = title + ((title == title.ToUpper()) ? "_H" : "_L");
            try
            {
                string etS = newEtalon.Aggregate("", (current, x) => current + x.ToString() + " ");
                using (FileStream stream = new FileStream("../../Etalons/" + title, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    StreamWriter writer = new StreamWriter(stream);
                    writer.WriteLine(etS);
                    writer.Close();
                }
            }
            catch (Exception exc)
            {
                if (isTracked) MessageBox.Show(exc.Message);
            }
        }

        #endregion

        #region UI

        /// <summary>
        /// Очищает PictureBox со входным изображением
        /// </summary>
        private void toClearInput()
        {
            Graphics g = imgInput.CreateGraphics();
            Pen pen = new Pen(Color.Black);
            Brush brush = Brushes.Black;

            g.Clear(Color.White);

            for (int i = 0; i < ImageHeight; i++)
            {
                for (int j = 0; j < ImageWidth; j++)
                {
                    g.DrawRectangle(pen, j * _pixelSize, i * _pixelSize, _pixelSize, _pixelSize);
                }
            }
        }

        /// <summary>
        /// Очищает PictureBox с выходным изображением
        /// </summary>
        private void toClearOutput()
        {
            Graphics g = imgOutput.CreateGraphics();
            Pen pen = new Pen(Color.Black);
            Brush brush = Brushes.Black;

            g.Clear(Color.White);

            for (int i = 0; i < ImageHeight; i++)
            {
                for (int j = 0; j < ImageWidth; j++)
                {
                    g.DrawRectangle(pen, j * _pixelSize, i * _pixelSize, _pixelSize, _pixelSize);
                }
            }
        }
               
        /// <summary>
        /// Рисует входное изображение
        /// </summary>
        /// <param name="e"> Вектор-вход </param>
        private void toDrawSymbol(double[] e)
        {
            toClearInput();

            Brush brush = Brushes.Black;
            Graphics g = imgInput.CreateGraphics();

            int j, k;
            for (int i = 0; i < e.Length; i++)
            {
                if (e[i] > 0)
                {
                    j = i % (ImageWidth);
                    k = i / (ImageHeight);

                    g.FillRectangle(brush, j * _pixelSize, k * _pixelSize, _pixelSize, _pixelSize);
                }
            }
        }

        /// <summary>
        /// Рисует выходное изображение
        /// </summary>
        /// <param name="e"> Вектор-выход </param>
        private void toDrawResult(double[] e)
        {
            toClearOutput();

            Brush brush = Brushes.Black;
            Graphics g = imgOutput.CreateGraphics();

            int j, k;
            for (int i = 0; i < e.Length; i++)
            {
                if (e[i] > 0)
                {
                    j = i % (ImageWidth);
                    k = i / (ImageHeight);

                    g.FillRectangle(brush, j * _pixelSize, k * _pixelSize, _pixelSize, _pixelSize);
                }
            }
        }
        #endregion

        #region Controls event handlers

        private void lstViewEtalons_SelectedIndexChanged(object sender, EventArgs e)
        {
            _input = _etalons.ElementAt(lstViewEtalons.SelectedIndex).Copy();
            toDrawSymbol(_input);
        }

        private void cmdSaveImage_Click(object sender, EventArgs e)
        {
            var newEtalon = _input.Copy();
            this._etalons.Add(newEtalon);
            string title = PromptDialog.ShowDialog("Введите название нового эталона", "Создание эталона");
            lstViewEtalons.Items.Add(title);
            lstViewEtalons.SelectedIndex = lstViewEtalons.Items.Count - 1;
            toDrawSymbol(_input);
            toSaveEtalonIntoFile(title, newEtalon, true);
        }

        private void cmdClear_MouseClick(object sender, MouseEventArgs e)
        {
            toClearInput();
            for (int i = 0; i < _input.Length; i++)
            {
                _input[i] = -1;
            }
            toDrawSymbol(_input);
        }

        // Клик по полю со входным рисунком
        public void imgInput_MouseClick(object sender, MouseEventArgs e)
        {
            toClearInput();
            int i = e.Y / _pixelSize;
            int j = e.X / _pixelSize;
            int k = i * (ImageHeight) + j;

            if (_input[k] > 0)
            {
                _input[k] = -1.0;
            }
            else
            {
                _input[k] = 1.0;
            }

            //toClear();
            toDrawSymbol(_input);
        }

        private void numWidth_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numeric = (sender as NumericUpDown);
            this.ImageWidth = (int)(numeric.Value);

            UpDownBase prev = (sender as UpDownBase);
            if (numeric.Value > int.Parse(prev.Text))
            {
                _input = _input.AddColumn((int)numeric.Value - 1).Copy();
                for (int i = 0; i < _etalons.Count; i++)
                {
                    var et = _etalons[i];
                    var etNew = et.AddColumn((int)numeric.Value - 1).Copy();
                    _etalons.Remove(et);
                    _etalons.Insert(i, etNew);
                }
            }
            else
            {
                _input = _input.RemoveColumn((int)numeric.Value - 1).Copy();
                for (int i = 0; i < _etalons.Count; i++)
                {
                    var et = _etalons[i];
                    var etNew = et.RemoveColumn((int)numeric.Value - 1).Copy();
                    _etalons.Remove(et);
                    _etalons.Insert(i, etNew);
                }
            }

            toMoveControlsAfterRecising();
            toDrawSymbol(_input);
        }

        private void numHeight_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown numeric = (sender as NumericUpDown);
            this.ImageHeight = (int) (numeric.Value);

            UpDownBase prev = (sender as UpDownBase);
            if (numeric.Value > int.Parse(prev.Text))
            {
                _input = _input.AddRow((int) numeric.Value-1).Copy();
                for (int i = 0; i < _etalons.Count; i++)
                {
                    var et = _etalons[i];
                    var etNew = et.AddRow((int) numeric.Value-1).Copy();
                    _etalons.Remove(et);
                    _etalons.Insert(i, etNew);
                }
            }
            else
            {
                _input = _input.RemoveRow((int)numeric.Value - 1).Copy();
                for (int i = 0; i < _etalons.Count; i++)
                {
                    var et = _etalons[i];
                    var etNew = et.RemoveRow((int)numeric.Value - 1).Copy();
                    _etalons.Remove(et);
                    _etalons.Insert(i, etNew);
                }
            }

            toMoveControlsAfterRecising();
            toDrawSymbol(_input);
        }

        private void toMoveControlsAfterRecising()
        {
            imgInput.Width = ImageWidth*_pixelSize;
            imgInput.Height = ImageHeight*_pixelSize;
            groupBox1.Width = imgInput.Width + cmdStart.Width + 3*_pixelSize;
            groupBox1.Height = imgInput.Height + 2*_pixelSize;
            this.Width = imgInput.Width + cmdStart.Width + 4*_pixelSize;
            numWidth.Left =
                numHeight.Left =
                lstViewEtalons.Left =
                cmdClear.Left =
                cmdSaveImage.Left =
                cmdStart.Left =
                imgInput.Width + 2*_pixelSize;


            //imgInput.Height = ImageHeight*_pixelSize + _pixelSize;
            //groupBox1.Height = Math.Max(imgInput.Height + 10, 100);
            //imgOutput.Top = imgInput.Height + _pixelSize;
            //cmdStart.Top = imgOutput.Top + (imgOutput.Height - cmdStart.Height/2);

            imgOutput.Top = groupBox1.Top + groupBox1.Height + _pixelSize;
            imgOutput.Size =imgInput.Size;
            this.Height = groupBox1.Height + imgOutput.Height + 3*_pixelSize + SystemInformation.MenuHeight;
        }

        #endregion

        #region Хебб

        private void TeachHabb()
        {
            _weights = new double[_input.Length + 1];
            int different = 0;

            do
            {
                // 1.
                different = 0;
               
                // 2.
                for (int i = 0; i < _etalons.Count; i++)
                {
                    // 2.2
                    int y = (i == 0) ? 1 : -1;

                    // 2.3
                    _weights[0] += y;
                    for (int j = 1; j < _weights.Length; j++)
                    {
                        _weights[j] += _etalons[i][j - 1] * y;
                    }

                    // 3. 
                    double s = _weights[0] + _etalons[i].Select((t, j) => t * _weights[j + 1]).Sum();
                    int ans = (s > 0) ? 1 : -1;
                    if (ans != y) different++;
                }
            } while (different != 0);
        }

        public void cmdStart_Click(object sender, EventArgs e)
        {
            // Инициализация и очищение
            //toInitEtalons();
            _output = new double[_input.Length];

            toDrawSymbol(_input);   // отрисовка исходных данных

            for (int i = 0; i < _input.Length; i++)
            {
                if (_input[i] < 0.2)
                {
                    _input[i] = -1;
                }

                _output[i] = -1;
            }

            // Работа
            TeachHabb();
            double s = _weights[0] + _input.Select((t, j) => t * _weights[j + 1]).Sum();
            int index = (s > 0) ? 0 : 1;
            _output = _etalons[index].Copy();
            //MessageBox.Show("Распознан образ: " + lstViewEtalons.Items[index]);

            // Вывод
            toDrawResult(_output);  // отрисовка результата            
        }
        
        #endregion
    }
}